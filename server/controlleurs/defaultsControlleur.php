<?php

	/**
	* 
	*/
	class defaultsControlleur extends controlleur
	{
		
		function __construct()
		{
			Posts::disableCSRF();
		}

		function index(){
			echo 'Something about the name.';
		}

		function suscribe()
		{
			$name = Posts::post('name');
			$mail = Posts::post('email');
			$uniq = md5(uniqid().date('dmYHis'));

			// create conversation
			file_put_contents(ROOT.'conversations/'.$uniq, json_encode([
				"online" => false,
				"messages" => [[
					"sender" => "admin",
					"content" => "Bienvenue ! <br>Je suis l'administrateur. En quoi pourrais-je vous être utile ?"
				]]
			]));

			// create user
			$users = json_decode(file_get_contents(ROOT.'conversations/users.json'), true);
			$users[$uniq] = [
				"name" => $name,
				"email" => $mail,
				"clientid" => $uniq
			];
			file_put_contents(ROOT.'conversations/users.json', json_encode($users));

			// feeding back answer
			$this->json_answer([
				"error" => false,
				"uniq" => $uniq
			]);
		}

		function getMessages()
		{
			$user = Posts::get('user');

			if (file_exists(ROOT.'conversations/'.$user)) {
				$all = json_decode(file_get_contents(ROOT.'conversations/'.$user), true);
				$all['messages'] = array_slice($all['messages'], Posts::get('last'));

				$line = json_decode(file_get_contents(ROOT.'conversations/online.json'), true);
				$line[$user] = date("H-i-s-m-d-Y");

				$adminlastseen = isset($line['admin']) ? $line['admin'] : '00-00-00-01-01-1970';
				$actualtime    = $line[$user];

				$all['adminisonline'] = $this->checkSomeoneConnected($adminlastseen, $actualtime);

				file_put_contents(ROOT.'conversations/online.json', json_encode($line));
				$this->json_answer($all);
			}
			else {
				$this->removeUser($user);
				$this->json_answer([
					"error" => true
				]);
			}
		}

		function getAll()
		{
			$return = [];
			
			// getting pokers
			$return['pokers'] = json_decode(file_get_contents(ROOT.'conversations/adminpokes.json'), true);
			
			// getting users
			$return['users'] = json_decode(file_get_contents(ROOT.'conversations/users.json'), true);
			
			// getting online
			$connected = json_decode(file_get_contents(ROOT.'conversations/online.json'), true);
			
			// getting conversing messages
			if (Posts::get(['client'])) {
				$msgs = json_decode(file_get_contents(ROOT.'conversations/'.Posts::get('client')), true);
				$msgs = array_slice($msgs['messages'], Posts::get('last'));

				$return['messages'] = $msgs;
				$return['pokers'][Posts::get('client')] = 0;

				// update adminpokes
				$new = json_decode(file_get_contents(ROOT.'conversations/adminpokes.json'), true);
				$new[Posts::get('client')] = 0;
				file_put_contents(ROOT.'conversations/adminpokes.json', json_encode($new));
			}

			// who are connected
			$now = date('H-i-s-m-d-Y');
			$checked = [];
			foreach ($connected as $k => $v) {
				$checked[$k] = $this->checkSomeoneConnected($now, $v);
			}
			$return['usersonline'] = $checked;

			// admin: am online
			$connected['admin'] = date('H-i-s-m-d-Y');
			file_put_contents(ROOT.'conversations/online.json', json_encode($connected));

			$this->json_answer($return);
		}

		function newMessage()
		{
			$user = Posts::post('user');
			$message = Posts::post('message');

			$msgs = json_decode(file_get_contents(ROOT.'conversations/'.$user), true);
			$poke = json_decode(file_get_contents(ROOT.'conversations/adminpokes.json'), true);

			$msgs['messages'][] = [
				"sender" => Posts::post(['admin']) ? 'admin' : $user,
				"content" => $message
			];
			

			if (file_put_contents(ROOT.'conversations/'.$user, json_encode($msgs))) {
				// poke admin
				if (!Posts::post(['admin'])) {
					$poke[$user] = isset($poke[$user]) ? $poke[$user]+1 : 1;
					if (file_put_contents(ROOT.'conversations/adminpokes.json', json_encode($poke))) {
						$this->json_answer(["error" => false]);
					}
					else {
						$this->json_answer(["error" => true]);
					}
				}
			}
			else {
				$this->json_answer(["error" => true]);
			}
		}

		function params()
		{
			$this->json_answer(json_decode(file_get_contents(ROOT.'conversations/admin.json'), true));
		}

		function setParams()
		{
			$return = [
				"error" => false,
				"message" => "Update done !"
			];
			if (!is_numeric(Posts::post('speed')) || Posts::post('speed')<500) {
				$return['error'] = true;
				$return['message'] = "Invalid update speed !";
				$this->json_answer($return);
			}
			else if (!is_numeric(Posts::post('maxfilesize'))) {
				$return['error'] = true;
				$return['message'] = "Invalid max file size !";
				$this->json_answer($return);
			}
			else {
				$filetype = explode(',', Posts::post('filetype'));
				$params = json_decode(file_get_contents(ROOT.'conversations/admin.json'), true);
				$params['welcome'] = Posts::post('welcomemessage');
				$params['display'] = Posts::post('displayname');
				$params['adminname'] = Posts::post('adminname');
				$params['speed'] = intval(Posts::post('speed'));
				$params['filetype'] = $filetype;
				$params['filesize'] = intval(Posts::post('maxfilesize'));

				if (file_put_contents(ROOT.'conversations/admin.json', json_encode($params))) {
					$this->json_answer($return);
				}
				else {
					$this->json_answer([
						"error" => true,
						"message" => 'Unable to update. Try again later.'
					]);
				}
			}
		}

		function updatePicture()
		{
			$file = Posts::file('profilepicture');
			$type = Posts::post('type');
			$name = "admin.".$type;
			$done = false;

			if (move_uploaded_file($file['tmp_name'], ROOT.'files/'.$name)) {
				$params = json_decode(file_get_contents(ROOT.'conversations/admin.json'), true);
				$params['logo'] = $name;

				if (file_put_contents(ROOT.'conversations/admin.json', json_encode($params))) {
					$done = true;
				}
			}

			return $done ? $this->json_answer([
				"error" => false,
				"message" => "Profile picture updated !",
				"file" => $name
			]) : $this->json_answer([
				"error" => true,
				"message" => "An error occured !"
			]);;
		}

		function uploadFile()
		{
			$file = Posts::file('newfile');
			$name = md5(date('dmYHis').uniqid()) . '.' . Posts::post('type');
			$destination = ROOT.'files/'.$name;

			if (move_uploaded_file($file['tmp_name'], $destination)) {
				$this->json_answer([
					"error" => false,
					"message" => "File sended !",
					"filename" => $name
				]);
			}
			else {
				$this->json_answer([
					"error" => true,
					"message" => "an error occured !"
				]);
			}
		}

		private function checkSomeoneConnected($d1, $d2)
		{
			$date1 = $this->mktimeFromDate($d1);
			$date2 = $this->mktimeFromDate($d2);
			$distance = $this->getDistance($date1, $date2);
			
			return ($distance < 10);
		}

		private function removeUser($user)
		{
			if (file_exists(ROOT.'conversations/'.$user)) @unlink(ROOT.'conversations/'.$user);
			$adminpokes = json_decode(file_get_contents(ROOT.'conversations/adminpokes.json'), true);
			$online = json_decode(file_get_contents(ROOT.'conversations/online.json'), true);
			$users = json_decode(file_get_contents(ROOT.'conversations/users.json'), true);
			
			if (isset($adminpokes[$user])) unset($adminpokes[$user]);
			if (isset($users[$user])) unset($users[$user]);
			if (isset($online[$user])) unset($online[$user]);

			file_put_contents(ROOT.'conversations/adminpokes.json', json_encode($adminpokes));
			file_put_contents(ROOT.'conversations/online.json', json_encode($online));
			file_put_contents(ROOT.'conversations/users.json', json_encode($users));
		}

		private function getDistance($d1, $d2)
		{
			return abs($d1 - $d2);
		}

		private function mktimeFromDate($date)
		{
			// les dates au format
			// H-i-s-m-d-Y
			$d = explode('-', $date);
			return mktime($d[0], $d[1], $d[2], $d[3], $d[4], $d[5]);
		}
	}