var olive = {
	displayname: null,

	adminlogo: null,

	buttontoggle: 0,

	viewstate: 0,

	chatviewstate: 0,

	attachboxstate: 0,

	emosboxstate: 0,

	ownmessagetemplate: '<div class="o-msg owner"><div class="content">{%= o.message %}{% if (o.image) { if (!/defaultfile.png/.test(o.image)) { %} <img src="{%= o.image %}"/> {% } else { %} <a href="{%= o.file %}" target="_blank"><i class="fa fa-file-zip-o"></i></a> {% }} %}</div></div>',

	othermessagetemplate: '<div class="o-msg other"><div class="thumb"><img src="{%= o.thumb %}"></div><div class="content">{%= o.message %}{% if (o.image) { if (!/defaultfile.png/.test(o.image)) { %} <img src="{%= o.image %}"/> {% } else { %} <a href="{%= o.file %}" target="_blank"><i class="fa fa-file-zip-o"></i></a> {% }} %}</div></div>',

	isAdmin: null,

	uniqid: null,

	updatetime: 2000,

	watcher: {interval: null, isbusy: false},

	messagesNumber: 0,

	unreadMessage: 0,

	acceptedtype: [],

	maxfilesize: 1000000,

	alertsong: null,

	path: null,

	togglebutton: function () {
		if (!olive.buttontoggle) {
			$('.o-trigger').animate({bottom: 0, opacity: 0}, 300, 'easeInBack', function () { $(this).css({zIndex: '-1'}) });
			$('.o-box').animate({bottom: 0, opacity: 1}, 300, 'easeInBack', function () { $(this).css({zIndex: '100000000000'}) });
			olive.buttontoggle = 1;
			olive.setUnread(0);
		}
		else {
			$('.o-box').animate({bottom: '50px', opacity: 0}, 300, 'easeInBack', function () { $(this).css({zIndex: '-1'}) });
			$('.o-trigger').animate({bottom: '50px', opacity: 1}, 300, 'easeOutBounce', function () { $(this).css({zIndex: '100000000000'}) });
			olive.buttontoggle = 0;
		}
	},

	toggleViews: function () {
		if (!olive.viewstate) {
			$('.o-body .o-first').animate({left: '-340px', opacity: 0}, 300, 'easeInCubic', function () { $(this).hide() });
			olive.viewstate = 1;
		}
		else {
			$('.o-body .o-first').show().animate({left: '0px', opacity: 1}, 300, 'easeInCubic');
			olive.viewstate = 0;
		}
	},

	alert: {
		error: function (message) {
			$('.o-alerter').addClass('error').text(message).show();
			setTimeout(function () { $('.o-alerter').hide(); }, 3000);
		},

		success: function (message) {
			$('.o-alerter').removeClass('error').text(message).show();
			setTimeout(function () { $('.o-alerter').hide(); }, 3000);
		}
	},

	getContent: function (path) {
		return $.get({
            url: path
        });
	},

	scrollView: function () {
		$('.o-chat').animate({scrollTop: $(".o-chat").offset().top+10000}, 500, 'easeInCubic');
	},

	createNewMessage: function(type, message) {
		var parts = /[img|file|emos]:[a-zA-Z0-9.]+/.test(message) ? message.split(':') : false;
		var image = null, file = null, emos = null;

		if (parts) {
			image = parts[0] == 'img' || parts[0] == 'emos' ? ((parts[0] == 'img' ? olive.path + 'server/files/' + parts[1] : (parts[0] == 'emos') ? olive.path + 'assets/emos/' + parts[1] : olive.path + 'server/files/defaultfile.png' )) : olive.path + 'server/files/defaultfile.png';
			file = parts[0] != 'img' ? olive.path + 'server/files/' + parts[1] : null;
		}

		var	body = image ? '' : message;

		if (type == 'own') {
			var data = {message: body, image: image, file: file};
			$('.o-chat').append(tmpl(olive.ownmessagetemplate, data));
		}
		else {
			var data = {message: body, image: image, file: file, thumb: olive.path + 'server/files/client.png'};
			$('.o-chat').append(tmpl(olive.othermessagetemplate, data));
		}
		olive.scrollView();
	},

	sendWrote: function (text) {
		var content = text || $('.o-wrote').val();

		if (!content.trim().length) return;

		olive.createNewMessage('own', content);
		$('.o-wrote').val(text ? $('.o-wrote').val() : '');

		$.post({
			url: olive.path + 'server/?/new-message',
			data: {user: olive.uniqid, message: content}
		})
		.done(function (datas) {
			if (datas.error) {
				// console.log("error while sending");
			}
			else {
				olive.messagesNumber += 1;
			}
		});
	},

	suscribe: function (name, email) {
		if (name.length && email.length) {
			var user = {name: name, email: email};
			
			$.post({
				url: olive.path + 'server/?/suscribe',
				data: user,
				dataType: 'json'
			})
			.done(function (datas) {
				if (!datas.error) {
					user.uniq = datas.uniq;
					olive.uniqid = datas.uniq;
					window.localStorage.setItem("olive", JSON.stringify(user));
					olive.toggleViews();
					olive.userStartWatching();
				}
			});
		}
		else {
			olive.alert.error('Renseignez votre nom et email');
		}
	},

	reset: function () {
		olive.stopWatching();
		olive.toggleViews();
		localStorage.removeItem('olive');
	},

	userGetMessages: function () {
		var userid = olive.uniqid;
				
		olive.watcher.isbusy = true;
		$.get({
			url: olive.path + 'server/?/get-messages/user='+olive.uniqid+'&last='+olive.messagesNumber,
			dataType: 'json'
		})
		.done(function (datas) {
			// if was not found
			if (datas.error) {
				olive.reset();
				return;
			}
			// then analysing answer
			if (datas.messages && datas.messages.length) {
				olive.onnewmessages(datas.messages, olive.messagesNumber ? 0 : 1);
				olive.setUnread(datas.messages.length);
				olive.showNumber(olive.unreadMessage);
				olive.messagesNumber += datas.messages.length;
			}
			else {
				olive.showNumber(olive.unreadMessage);
			}

			olive.setAdminOnline(datas.adminisonline);
			olive.watcher.isbusy = false;
		})
		.fail(function () {
			olive.watcher.isbusy = false;
		});
	},

	fillMessageBox: function (messages) {
		for (var i=0; i<messages.length; i++) {
			if (olive.messagesNumber) {
				if (messages[i].sender != olive.uniqid) {
					olive.createNewMessage('other', messages[i].content);
				}
			}
			else {
				olive.createNewMessage(messages[i].sender==olive.uniqid ? 'own' : 'other', messages[i].content);
			}
		}
	},

	setAdminOnline: function (online) {
		if (online != olive.isAdmin) {
			if (online) {
				$('.o-two .adminstatus').text('Admin is online').fadeIn();
				setTimeout(function () {
					$('.o-two .adminstatus').fadeOut();
				}, 3000);
			}
			else {
				$('.o-two .adminstatus').text('Admin is offline').fadeIn();
			}
			olive.isAdmin = online;
		}
	},

	setUnread: function (number) {
		if (olive.messagesNumber) {
			olive.unreadMessage = olive.buttontoggle ? 0 : olive.unreadMessage + number;
		}
	},

	showNumber: function (number) {
		if (olive.messagesNumber) {
			$('.o-trigger .o-new').text(number).css({display: (number ? 'block' : 'none')});
			// console.log(number);
		}
	},

	userStartWatching: function () {
		if (olive.watcher.interval) return;

		olive.watcher.interval = setInterval(function () {
			if (!olive.watcher.isbusy) {
				olive.userGetMessages();
			}
		}, olive.updatetime);
	},

	stopWatching: function () {
		olive.watcher.isbusy = false;
		clearInterval(olive.watcher.interval);
		olive.watcher.interval = null;
	},

	loadParams: function (loaded) {
		$.get({
			url: olive.path + 'server/?/params',
			dataType: 'json'
		})
		.done(function (datas) {
			olive.displayname = datas.display;
			olive.adminlogo = datas.logo;
			olive.updatetime = datas.speed;
			olive.maxfilesize = datas.filesize;
			olive.acceptedtype = datas.filetype;
			olive.alertsong = datas.alertsong;
			olive.updateValues();
			if (loaded) loaded();
		});
	},

	updateValues: function () {
		$('.o-head .o-box-title').text(olive.displayname);
		$('.o-first .o-logo img').attr({src: olive.path + "server/files/"+olive.adminlogo});
		$('.o-live audio').attr({src: olive.path + "assets/song/" + olive.alertsong});
	},

	displayToggler: function (on) {
		on = on==undefined ? true : on;
		$('.o-trigger').css({display: on ? 'block' : 'none'});
	},

	playAlert: function () {
		document.getElementById('alertplayer').play();
	},

	onnewmessages: function (messages, firsttime) {
		olive.fillMessageBox(messages);
		if (!firsttime) olive.playAlert();
	},

	toggleAttachBox: function (value) {
		if (!olive.attachboxstate) {
			$('div.o-body div.o-add-box').css({display: 'block'}).animate({opacity: 1, bottom: '63px'}, 100, 'easeInCubic');
			$(' div.o-write button.o-add-file').css({transform: 'rotate(45deg)'});
		}
		else {
			$('div.o-body div.o-add-box').animate({opacity: 0, bottom: '50px'}, 100, 'easeInCubic', function () {
				$(this).css({display: 'none'});
			});
			$(' div.o-write button.o-add-file').css({transform: 'rotate(0deg)'});
			if (olive.emosboxstate) olive.closeEmosBox();
		}
		olive.attachboxstate = !olive.attachboxstate;
	},

	toggleEmosBox: function () {
		if (!olive.emosboxstate) {
			olive.openEmosBox();
		}
		else {
			olive.closeEmosBox();
		}
	},

	openEmosBox: function () {
		$('div.o-body div.o-add-box .emos-box').animate({height: '200px'}, 300, 'easeOutCubic');
		olive.emosboxstate = !olive.emosboxstate;
	},

	closeEmosBox: function () {
		$('div.o-body div.o-add-box .emos-box').animate({height: '0px'}, 100, 'easeOutCubic');
		olive.emosboxstate = !olive.emosboxstate;
	},

	sendEmos: function (link) {
		var emos = link.split('/'); emos = emos[emos.length -1];
		olive.sendWrote('emos:' + emos);
		olive.toggleAttachBox();
	},

	progressBar: function (loaded, total) {
		var percent = (loaded*100)/total;
		// $('div.o-progress-bar').css({opacity: 1; display: 'block'});
		$('div.o-progress-bar div.thebar').animate({width: percent + '%'});
	},

	resetProgressBar: function () {
		setTimeout(function () {
			$('div.o-progress-bar div.thebar').css({width: '0px'});
		}, 3000);
	},

	sendFile: function (file) {
		var filetype = file.type.split('/')[1].toLowerCase(), filesize = file.size;

		if (olive.acceptedtype.indexOf(filetype) == -1) {
			olive.alert.error("File type '" + filetype + "' not allowed !");
		}
		else if (filesize > olive.maxfilesize) {
			olive.alert.error("File size bigger than max allowed size !");
		}
		else {
			var data = new FormData();
				data.append('newfile', file);
				data.append('type', filetype);
				data.append('size', filesize);

			$.ajax({
				url: olive.path + 'server/?/upload-file',
				type: 'post',
                contentType: false,
                processData: false,
                data: data,
                dataType: 'json',
                xhr: function () {
                	var xhr = new XMLHttpRequest();
                	xhr.upload.addEventListener('progress', function (e) {
                		olive.progressBar(e.loaded, e.total);
                	});
                	return xhr;
                }
			})
			.done(function (datas) {
				if (datas.error) {
					olive.alert.error(datas.message);
				}
				else {
					var type = ['png', 'jpg', 'jpeg', 'gif', 'bmp'].indexOf(filetype) != -1 ? 'img' : 'file';
					olive.sendWrote(type + ':' + datas.filename);
				}
				olive.resetProgressBar();
			})
			.fail(function () {
				olive.alert.error("File sending failed !");
			});
		}
	}
};

$(function () {
	/* setting path */
	olive.path = window.olivepath;

	/* when closing or opening */
	$('.o-trigger, .o-box .o-box-closer').on('click', olive.togglebutton);

	/* when submit suscribe form */
	$('.o-suscribebtn').on('click', function (evt) {
		evt.preventDefault();
		olive.suscribe($('.o-user-name').val(), $('.o-user-mail').val());
	});

	/* when sending from button */
	$('.o-send').on('click', function () {
		olive.sendWrote();
	});

	/* when send from key */
	$('.o-wrote').on('keyup', function (evt) {
		evt.preventDefault();
		if (evt.which == 13 && !evt.shiftKey) olive.sendWrote();
	});

	/* when toggling file adding box */
	$('div.o-write button.o-add-file').on('click', function () {
		olive.toggleAttachBox();
	});

	/* when choosing a file */
	$('.o-add-box #sharefile').on('change', function () {
		olive.toggleAttachBox();
		olive.sendFile(this.files[0]);
	});

	/* when sending emoji */
	$('.o-add-box .emos-box .content .o-emos').on('click', function () {
		olive.sendEmos(this.src);
	});

	/* show the view trigger button */
	olive.loadParams(function () {
		/* automaticaly toggle views */
		if (window.localStorage.getItem('olive')) {
			olive.uniqid = JSON.parse(window.localStorage.getItem('olive')).uniq;
			olive.toggleViews();
			olive.userStartWatching();
		}
		olive.displayToggler();
	});
});