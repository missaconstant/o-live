var olive = {
	displayname: null,

	adminname: null,

	adminlogo: null,

	welcomemessage: null,

	buttontoggle: 0,

	viewstate: 0,

	chatviewstate: 0,

	attachboxstate: 0,

	emosboxstate: 0,

	ownmessagetemplate: '<div class="o-msg owner"><div class="content">{%= o.message %}{% if (o.image) { if (!/defaultfile.png/.test(o.image)) { %} <img src="{%= o.image %}"/> {% } else { %} <a href="{%= o.file %}" target="_blank"><i class="fa fa-file-zip-o"></i></a> {% }} %}</div></div>',

	othermessagetemplate: '<div class="o-msg other"><div class="thumb"><img src="{%= o.thumb %}"></div><div class="content">{%= o.message %}{% if (o.image) { if (!/defaultfile.png/.test(o.image)) { %} <img src="{%= o.image %}"/> {% } else { %} <a href="{%= o.file %}" target="_blank"><i class="fa fa-file-zip-o"></i></a> {% }} %}</div></div>',

	isAdmin: 0,

	uniqid: null,

	updatetime: 2000,

	watcher: {interval: null, isbusy: false, updating: false},

	messagesNumber: 0,

	messageOpened: 0,

	openedUserState: null,

	unreadMessage: 0,

	acceptedtype: [],

	maxfilesize: 1000000,

	alertsong: null,

	path: null,

	togglebutton: function () {
		olive.toggleBox(olive.buttontoggle);
	},

	toggleBox: function (opened) {
		if (!opened) {
			$('.o-trigger').animate({bottom: 0, opacity: 0}, 300, 'easeInBack', function () { $(this).css({zIndex: '-1'}) });
			$('.o-box').animate({bottom: 0, opacity: 1}, 300, 'easeInBack', function () { $(this).css({zIndex: '100000000000'}) });
			olive.buttontoggle = 1;
		}
		else {
			$('.o-box').animate({bottom: '50px', opacity: 0}, 300, 'easeInBack', function () { $(this).css({zIndex: '-1'}) });
			$('.o-trigger').animate({bottom: '50px', opacity: 1}, 300, 'easeOutBounce', function () { $(this).css({zIndex: '100000000000'}) });
			olive.buttontoggle = 0;

			setTimeout(function () {
				olive.toggleChatViews(null, olive.messageOpened);
			}, 300);
		}
	},

	alert: {
		error: function (message) {
			$('.o-alerter').addClass('error').text(message).show();
			setTimeout(function () { $('.o-alerter').hide(); }, 3000);
		},

		success: function (message) {
			$('.o-alerter').removeClass('error').text(message).show();
			setTimeout(function () { $('.o-alerter').hide(); }, 3000);
		}
	},

	getContent: function (path) {
		return $.get({
            url: olive.path + path
        });
	},

	scrollView: function () {
		$('.o-chat').animate({scrollTop: $(".o-chat").offset().top+10000}, 500, 'easeInCubic');
	},

	createNewMessage: function(type, message) {
		var parts = /[img|file|emos]:[a-zA-Z0-9.]+/.test(message) ? message.split(':') : false;
		var image = null, file = null, emos = null;

		if (parts) {
			image = parts[0] == 'img' || parts[0] == 'emos' ? ((parts[0] == 'img' ? olive.path + 'server/files/' + parts[1] : (parts[0] == 'emos') ? olive.path + 'assets/emos/' + parts[1] : olive.path + 'server/files/defaultfile.png' )) : olive.path + 'server/files/defaultfile.png';
			file = parts[0] != 'img' ? olive.path + 'server/files/' + parts[1] : null;
		}

		var	body = image ? '' : message;

		if (type == 'own') {
			var data = {message: body, image: image, file: file};
			$('.o-chat').append(tmpl(olive.ownmessagetemplate, data));
		}
		else {
			var data = {message: body, image: image, file: file, thumb: olive.path + 'server/files/client.png'};
			$('.o-chat').append(tmpl(olive.othermessagetemplate, data));
		}
		olive.scrollView();
	},

	createNewUser: function (user) {
		olive.getContent('templates/clientlist.tpl.html').done(function (content) {
			user.thumb = olive.path + 'assets/images/person.png';
			$('.o-parts .users-list ul').append(tmpl(content, user));
		});
	},

	sendWrote: function (text) {
		var content = text || $('.o-wrote').val();

		if (!content.trim().length) return;

		olive.createNewMessage('own', content);
		$('.o-wrote').val(text ? $('.o-wrote').val() : '');

		$.post({
			url: olive.path + 'server/?/new-message',
			data: {admin: 'true', user: olive.messageOpened, message: content}
		})
		.done(function (datas) {
			if (datas.error) {
				console.log("error while sending");
			}
			else {
				olive.messagesNumber += 1;
			}
		});
	},

	adminGetAll: function () {
		var userid = olive.uniqid;
				
		olive.watcher.isbusy = true;
		$.get({
			url: olive.path + 'server/?/get-all'+( olive.messageOpened ? '/last='+olive.messagesNumber+'&client='+olive.messageOpened : ''),
			dataType: 'json'
		})
		.done(function (datas) {
			// updating conversation
			if (olive.messageOpened) {
				if (datas.messages && datas.messages.length) {
					olive.onnewmessages(datas.messages, olive.messagesNumber ? 0 : 1);
					olive.messagesNumber += datas.messages.length;
				}
			}
			// adding users
			olive.fillUserBox(datas.users);

			// poker incrementing
			olive.fillPokers(datas.pokers);

			// online settings
			olive.setOnlineUsers(datas.usersonline);

			olive.watcher.isbusy = false;
		})
		.fail(function () {
			olive.watcher.isbusy = false;
		});
	},

	fillMessageBox: function (messages) {
		for (var i=0; i<messages.length; i++) {
			if (olive.messagesNumber) {
				if (messages[i].sender != olive.uniqid) {
					olive.createNewMessage('other', messages[i].content);
				}
			}
			else {
				olive.createNewMessage(messages[i].sender==olive.uniqid ? 'own' : 'other', messages[i].content);
			}
		}
	},

	fillUserBox: function (users) {
		for (var user in users) {
			if (olive.clients) {
				if (!olive.clients[user]) {
					olive.createNewUser(users[user]);
					olive.clients[user] = users[user];
				}
			}
			else {
				olive.clients = {};
				olive.createNewUser(users[user]);
				olive.clients[user] = users[user];
			}
		}
	},

	fillPokers: function (pokers) {
		var ispoker = false, unread = 0;
		olive.pokers = olive.pokers ? olive.pokers : {};
		
		var samepokers = JSON.stringify(olive.pokers) == JSON.stringify(pokers);


		if (olive.pokers) {
			for (var poker in pokers) {
				$poker = $('.o-parts .users-list ul li#'+poker).find('span.newest');
				$poker.text(pokers[poker]);
				$poker.css({display: pokers[poker] ? 'block' : 'none'});

				olive.pokers = pokers;
				ispoker = pokers[poker] ? true : ispoker;

				unread = !samepokers ? olive.addPokers(pokers) : olive.unreadMessage;
			}
		}

		olive.messageLight(ispoker);
		olive.setUnread(unread);
		olive.showNumber(olive.unreadMessage);
	},

	addPokers: function (pokers) {
		var sum = 0;

		for (var a in pokers) {
			sum += pokers[a];
		}

		return sum;
	},

	messageLight: function (on) {
		$('.o-controls li[data-part="chat"] .smal').css({display: on ? 'block' : 'none'});
	},

	setOnlineUsers: function (usersonline) {
		$listElements = $('.o-parts .users-list ul li');
		$.each($listElements, function (index, element) {
			var isonline = usersonline[element.id] ? true : false;
			$(element).find('span.online').css({display: isonline ? 'block' : 'none'});

			if (element.id == olive.messageOpened && isonline != olive.openedUserState) {
				if (isonline) {
					$('.part-chat .adminstatus').text('This user is online').fadeIn();
					setTimeout(function () {
						$('.part-chat .adminstatus').fadeOut();
					}, 3000);
				}
				else {
					$('.part-chat .adminstatus').text('This user is offline').fadeIn();
				}

				olive.openedUserState = isonline;
			}
		});
	},

	setUnread: function (number) {
		if (olive.pokers) {
			olive.unreadMessage = number;
		}
	},

	showNumber: function (number) {
		if (olive.pokers) {
			$('.o-trigger .o-new').text(number).css({display: (number ? 'block' : 'none')});
		}
	},

	adminStartWatching: function () {
		if (olive.watcher.interval) return;

		olive.watcher.interval = setInterval(function () {
			if (!olive.watcher.isbusy) {
				olive.adminGetAll();
			}
		}, olive.updatetime);
	},

	stopWatching: function () {
		olive.watcher.isbusy = false;
		clearInterval(olive.watcher.interval);
		olive.watcher.interval = null;
	},

	loadParams: function (loaded) {
		$.get({
			url: olive.path + 'server/?/params',
			dataType: 'json'
		})
		.done(function (datas) {
			olive.displayname = datas.display;
			olive.adminname = datas.display;
			olive.adminlogo = datas.logo;
			olive.welcomemessage = datas.welcome;
			olive.updatetime = datas.speed;
			olive.maxfilesize = datas.filesize;
			olive.acceptedtype = datas.filetype;
			olive.alertsong = datas.alertsong;
			olive.updateValues();
			if (loaded) loaded();
		});
	},

	updateValues: function () {
		$('.o-live .o-head .o-box-title').text(olive.displayname);
		$('.o-live .adminpicture img').attr({src: olive.path + "server/files/"+olive.adminlogo});
		$('.o-live .o-admin-name h4').text(olive.adminname);
		$('.o-live .part-settings .inp-group input[name="adminname"]').val(olive.adminname);
		$('.o-live .part-settings .inp-group input[name="welcomemessage"]').val(olive.welcomemessage);
		$('.o-live .part-settings .inp-group input[name="displayname"]').val(olive.displayname);
		$('.o-live .part-settings .inp-group input[name="speed"]').val(olive.updatetime);
		$('.o-live .part-settings .inp-group input[name="filetype"]').val(olive.acceptedtype);
		$('.o-live .part-settings .inp-group input[name="maxfilesize"]').val(olive.maxfilesize);
		$('.o-live .part-about img.logo').attr({src: olive.path + "assets/images/logo.png"});
		$('.o-live audio').attr({src: olive.path + "assets/song/" + olive.alertsong});
		$('.o-live audio')[0].volume = 0.4;
	},

	updateParams: function () {
		if (olive.watcher.updating) return;

		$form = $('.part-settings .o-update-settings');
		olive.watcher.updating = true;
		$.post({
			url: olive.path + 'server/?/set-params',
			data: $form.serialize(),
			dataType: 'json'
		})
		.done(function (datas) {
			olive.alert[datas.error ? 'error' : 'success'](datas.message);
			olive.watcher.updating = false;
		});
	},

	updateProfilePicture: function (file) {
		if (olive.updating) return;

		var filetype = file.type.split('/')[1].toLowerCase();
		console.log(filetype);

		if (["jpg","jpeg","bmp","gif","png"].indexOf(filetype) == -1) {
			olive.alert.error("Picture required !");
			$('#profilepicture').val('');
		}
		else {
			olive.updating = true;

			var form = new FormData();
				form.append('profilepicture', file);
				form.append('type', filetype);

			$.ajax({
				url: olive.path + 'server/?/update-picture',
				type: 'post',
				cache: false,
                contentType: false,
                processData: false,
                data: form,
                dataType: 'json'
			})
			.done(function (datas) {
				var $image = document.querySelector('.part-home .adminpicture img');
				var src = datas.error ? $image.getAttribute('src') : olive.path + 'server/files/'+datas.file;

				olive.alert[datas.error ? 'error' : 'success'](datas.message);
				$image.setAttribute('src', src);
				olive.updating = false;
			});
		}
	},

	displayToggler: function (on) {
		on = on==undefined ? true : on;
		$('.o-trigger').css({display: on ? 'block' : 'none'});
	},

	onnewmessages: function (messages, firsttime) {
		olive.fillMessageBox(messages);
		if (!firsttime) olive.playAlert();
	},

	playAlert: function () {
		document.getElementById('alertplayer').play();
	},

	/* admin */

	switchViews: function (el) {
		$('.o-controls .o-active').removeClass('o-active');
		$('.o-parts.active').removeClass('active');
		$(this).addClass('o-active');
		$('.o-parts.part-'+$(this).attr('data-part')).addClass('active');
	},

	emptyMessagesView: function () {
		$('.o-parts .o-chat').html('');
	},

	toggleChatViews: function (userElement, closing) {
		if (!olive.chatviewstate && closing===undefined) {
			$('.o-parts .users-list').animate({left: '-100%'}, 300, 'easeInCubic');
			$('.o-parts .conversation').animate({opacity: 1}, 300, 'easeInCubic');
			olive.chatviewstate = 1;
			// conversation with
			olive.emptyMessagesView();
			olive.messageOpened = userElement.id;
			olive.messagesNumber = 0;
		}
		else if (olive.chatviewstate || closing) {
			$('.o-parts .users-list').animate({left: 0}, 300, 'easeInCubic');
			$('.o-parts .conversation').animate({opacity: 0}, 300, 'easeInCubic');
			olive.chatviewstate = 0;
			// close opened conversation
			olive.messageOpened = 0;
			olive.openedUserState = null;
		}
	},

	toggleAttachBox: function (value) {
		if (!olive.attachboxstate) {
			$('div.o-body div.o-add-box').css({display: 'block'}).animate({opacity: 1, bottom: '63px'}, 100, 'easeInCubic');
			$(' div.o-write button.o-add-file').css({transform: 'rotate(45deg)'});
		}
		else {
			$('div.o-body div.o-add-box').animate({opacity: 0, bottom: '50px'}, 100, 'easeInCubic', function () {
				$(this).css({display: 'none'});
			});
			$(' div.o-write button.o-add-file').css({transform: 'rotate(0deg)'});
			if (olive.emosboxstate) olive.closeEmosBox();
		}
		olive.attachboxstate = !olive.attachboxstate;
	},

	toggleEmosBox: function () {
		if (!olive.emosboxstate) {
			olive.openEmosBox();
		}
		else {
			olive.closeEmosBox();
		}
	},

	openEmosBox: function () {
		$('div.o-body div.o-add-box .emos-box').animate({height: '200px'}, 300, 'easeOutCubic');
		olive.emosboxstate = !olive.emosboxstate;
	},

	closeEmosBox: function () {
		$('div.o-body div.o-add-box .emos-box').animate({height: '0px'}, 100, 'easeOutCubic');
		olive.emosboxstate = !olive.emosboxstate;
	},

	sendEmos: function (link) {
		var emos = link.split('/'); emos = emos[emos.length -1];
		olive.sendWrote('emos:' + emos);
		olive.toggleAttachBox();
	},

	progressBar: function (loaded, total) {
		var percent = (loaded*100)/total;
		// $('div.o-progress-bar').css({opacity: 1; display: 'block'});
		$('div.o-progress-bar div.thebar').animate({width: percent + '%'});
	},

	resetProgressBar: function () {
		setTimeout(function () {
			$('div.o-progress-bar div.thebar').css({width: '0px'});
		}, 3000);
	},

	sendFile: function (file) {
		var filetype = file.type.split('/')[1].toLowerCase(), filesize = file.size;

		if (olive.acceptedtype.indexOf(filetype) == -1) {
			olive.alert.error("File type '" + filetype + "' not allowed !");
		}
		else if (filesize > olive.maxfilesize) {
			olive.alert.error("File size bigger than max allowed size !");
		}
		else {
			var data = new FormData();
				data.append('newfile', file);
				data.append('type', filetype);
				data.append('size', filesize);

			$.ajax({
				url: olive.path + 'server/?/upload-file',
				type: 'post',
                contentType: false,
                processData: false,
                data: data,
                dataType: 'json',
                xhr: function () {
                	var xhr = new XMLHttpRequest();
                	xhr.upload.addEventListener('progress', function (e) {
                		olive.progressBar(e.loaded, e.total);
                	});
                	return xhr;
                }
			})
			.done(function (datas) {
				if (datas.error) {
					olive.alert.error(datas.message);
				}
				else {
					var type = ['png', 'jpg', 'jpeg', 'gif', 'bmp'].indexOf(filetype) != -1 ? 'img' : 'file';
					olive.sendWrote(type + ':' + datas.filename);
				}
				olive.resetProgressBar();
			})
			.fail(function () {
				olive.alert.error("File sending failed !");
			});
		}
	}
};

$(function () {
	/* setting path */
	olive.path = window.olivepath;

	/* when closing or opening */
	$('.o-trigger, .o-box .o-box-closer').on('click', olive.togglebutton);

	/* when sending from button */
	$('.o-send').on('click', function () {
		olive.sendWrote();
	});

	/* when send from key */
	$('.o-wrote').on('keyup', function (evt) {
		evt.preventDefault();
		if (evt.which == 13 && !evt.shiftKey) olive.sendWrote();
	});

	/* switching between admin views */
	$('.o-controls ul li').on('click', olive.switchViews);

	/* when click on back btn in conversion */
	$('div.conversation span.backbtn').on('click', olive.toggleChatViews);

	/* when changing profile picture */
	$('#profilepicture').on('change', function (e) {
		olive.updateProfilePicture(this.files[0]);
	});

	/* when submiting update */
	$('.inp-group .o-update-submit').on('click', function (e) {
		e.preventDefault();
		olive.updateParams();
	});

	/* when toggling file adding box */
	$('div.o-write button.o-add-file').on('click', function () {
		olive.toggleAttachBox();
	});

	/* when choosing a file */
	$('.o-add-box #sharefile').on('change', function () {
		olive.toggleAttachBox();
		olive.sendFile(this.files[0]);
	});

	/* when sending emoji */
	$('.o-add-box .emos-box .content .o-emos').on('click', function () {
		olive.sendEmos(this.src);
	});

	/* show the trigger button and start watching */
	olive.loadParams(function () {
		olive.uniqid = "admin";
		olive.adminStartWatching();
		olive.displayToggler();
	});
});