var olive = {
	path: null,

	init: function (type, path) {
		olive.path = path + '/';
		window.olivepath = olive.path;
		window.inserCaret = olive.inserCaret;
		
		switch	(type) {
			case 'client':
				olive.display('client');
				break;

			case 'admin':
				olive.display('admin');
				break;
		}
	},

	createScript: function (type) {
		var script = document.createElement('script');
			script.type = 'text/javascript';
			script.src = olive.path + 'assets/js/' + (type=='admin' ? 'olive.admin.js' : 'olive.js');

		return script;
	},

	createTmpl: function () {
		var script = document.createElement('script');
			script.type = 'text/javascript';
			script.src = olive.path + 'assets/js/tmpl.js';

		return script;
	},

	createStyle: function () {
		var script = document.createElement('link');
			script.rel = 'stylesheet';
			script.href = olive.path + 'assets/css/style.css';

		return script;
	},

	createFont: function () {
		var script = document.createElement('link');
			script.rel = 'stylesheet';
			script.href = olive.path + 'assets/fonts/fa/css/font-awesome.min.css';

		return script;
	},

	createEmos: function () {
		var $emosbox = $('.o-add-box .emos-box .content');
			console.log($emosbox.length);
		for (var i=600; i<645; i++) {
			var $emo = new Image();
			$emo.className = 'o-emos';
			$emo.src = olive.path + '/assets/emos/1f' + i + '.png';
			$emo.onload = function (e) { $emosbox.append(this); };
		}
	},

	setStyles: function () {
		$("div.part-settings div.inp-group a.o-update-submit").css({
			display: "inline-block",
			width: "100%",
			padding: "12px",
			background: "#44A55B",
			"border-radius": "20px",
			color: "#fff",
			border: "1px solid #44A55B",
			"box-shadow": "0px 2px 7px 0px #ddd",
			"font-size": "13px",
			"box-sizing": "border-box",
			"cursor": "pointer"
		});
		$("div.inp-group input").css({
			background: "transparent",
			width: "100%",
			"box-sizing": "border-box",
			outline: "none",
			"border-radius": "20px",
			padding: "12px",
			border: "1px solid #ddd",
			"box-shadow": "0px 2px 7px 0px #eee",
			"font-size": "13px"
		});
		$("div.o-admin div.o-body div.o-controls ul li i.fa").css({
			"font-size": "17px"
		});
		$(".o-suscribebtn").css({
			"color": "#44A55B"
		});
		$("div.o-admin div.o-body div.part-settings div.inp-group label").css({
			"font-weight": "normal"
		});
		$('.o-live *:not(.o-trigger)').css({
			"line-height": "initial"
		});
		$('div.adminpicture label.editbtn').css({
			"line-height": "40px"
		});
		$('div.o-trigger .o-new').css({
			"line-height": "20px"
		});
		$('div.o-box div.o-body div.o-add-box').css({
			"padding": "5px"
		});
	},

	display: function (type) {
		var html = null;
		$.get({
			url: olive.path + 'templates/' + type + 'box.html',
			dataType: 'html'
		}).done(function (datas) {
			var div = document.createElement('div');
				div.className = 'o-live';
				div.style.display = 'none';
			var one = olive.createScript(type);
			var four = olive.createTmpl();
			var two = olive.createStyle();
			var three = olive.createFont();

			$(div).append(datas);
			$('body').append(div);
			$('head').append(two);
			$('head').append(three);
			olive.setStyles();
			olive.createEmos();

			two.onload = function () {
				$('body').append(four);
				$('body').append(one);
				$(div).show();
			}
		});
	},

	inserCaret: function (txtarea, text) {
			var scrollPos = txtarea.scrollTop;
			var strPos = 0;
			var br = ((txtarea.selectionStart || txtarea.selectionStart == '0') ? 
				"ff" : (document.selection ? "ie" : false ) );
			if (br == "ie") { 
				txtarea.focus();
				var range = document.selection.createRange();
				range.moveStart ('character', -txtarea.value.length);
				strPos = range.text.length;
			}
			else if (br == "ff") strPos = txtarea.selectionStart;
		
			var front = (txtarea.value).substring(0,strPos);  
			var back = (txtarea.value).substring(strPos,txtarea.value.length); 
			txtarea.value=front+text+back;
			strPos = strPos + text.length;
			if (br == "ie") { 
				txtarea.focus();
				var range = document.selection.createRange();
				range.moveStart ('character', -txtarea.value.length);
				range.moveStart ('character', strPos);
				range.moveEnd ('character', 0);
				range.select();
			}
			else if (br == "ff") {
				txtarea.selectionStart = strPos;
				txtarea.selectionEnd = strPos;
				txtarea.focus();
			}
			txtarea.scrollTop = scrollPos;
	}
};